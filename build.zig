const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const network_module = b.addModule("network", .{
        .root_source_file = .{ .path = "src/network.zig" },
        .target = target,
        .optimize = optimize,
    });

    const dns = b.addModule("dns", .{
        .root_source_file = .{ .path = "src/dns.zig" },
        .target = target,
        .optimize = optimize,
    });
    dns.addImport("network", network_module);
}
